import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;


class Program
{

   Profile profile = new Profile("a", "a", 1);

//   public String toString(){
//
////      return fir;
//   }

   static void tester() throws FileNotFoundException, UnsupportedEncodingException, JSONException {
      System.out.println("test");

      int count = 0;
      ArrayList<Profile> profiles = new ArrayList<>();
      Profile p = new Profile("Alex", "Adriaans", count++);
      p.addWeight(80.0, "gisteren");
      p.addWeight(90.1, "vandaag");
      p.addWeight(100.0, "morgen");
      profiles.add(p);

      p = new Profile("Alex", "Adriaans", count++);
      p.addWeight(80.0, "gisteren");
      p.addWeight(90.1, "vandaag");
      p.addWeight(100.0, "morgen");
      profiles.add(p);


      final String filename = "myprofiles.json";
      PrintWriter writer = new PrintWriter( filename, "UTF-8");

      String s;
      Program prog = new Program();
      for (Profile q : profiles )
      {
         s = q.toJSON().toString();
         writer.println( s );
      }

      writer.flush();
      writer.close();
      profiles.toString();
      System.out.println(profiles);
   }

   public static void main(String[] args ) throws Exception {

      ArrayList<Profile> profiles = new ArrayList<>();
      int count = 0;
      boolean save = false;

      Profile p = new Profile("Alex", "Adriaans", count++);
      p.addWeight(80.0, "gisteren");
      p.addWeight(90.1, "vandaag");
      p.addWeight(100.0, "morgen");
      profiles.add(p);

      Scanner scanner = new Scanner(System.in);
      int confirm = scanner.nextInt();
      if (confirm == 0) {
         save = false;
      }
      if (confirm == 1) {
         save = true;
      }
      if (confirm == 2) {
         //todo WIP
         String name = scanner.next();
         String surName = scanner.next();
         Double W1 = scanner.nextDouble();
         p = new Profile(name, surName, count++);
         p.addWeight(W1, "GISTEREN");
         p.addWeight(54.1, "VANDAAG");
         p.addWeight(54.0, "MORGEN");
         p.addWeight(54.0, "OVERMORGEN");
         profiles.add(p);
      }

      p = new Profile("Bas", "Boers", count++);
      p.addWeight(80.0, "gisteren");
      p.addWeight(90.1, "vandaag");
      p.addWeight(100.0, "morgen");
      profiles.add(p);

      p = new Profile("Celine", "Caalders", count++);
      p.addWeight(20.0, "Gisteren");
      p.addWeight(30.1, "Vandaag");
      p.addWeight(10.0, "Morgen");
      profiles.add(p);

      final String filename = "myprofiles.json";

      if ( save )
      {
         PrintWriter writer = new PrintWriter( filename, "UTF-8");

         String s;
         for (Profile q : profiles )
         {
            s = q.toJSON().toString();
            writer.println( s );
         }

         writer.flush();
         writer.close();
      }
      else
      {
         File file = new File( filename );
         if ( !file.exists() )
         {
              System.out.format( "ERROR: file %s does not exist.\n", filename );
              System.exit(0);
         }
         InputStream is = new FileInputStream( file );
         //InputStream is = Program.class.getResourceAsStream( filename );
         JSONTokener tokener = new JSONTokener(is);

         while (true)
         {
            try
            {
               JSONObject object = new JSONObject(tokener);
               Profile newProfile = new Profile( object );
               newProfile.print();
               System.out.println( "====================");
            }
            catch (org.json.JSONException e)
            {
               System.out.println( "end-of-input" );
               break;
            }
            catch (Exception e)
            {
               e.printStackTrace();
            }
         }
      }
   }
}
