import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

class Profile
{
   private String _firstName;
   private String _surName;
   private int   _id;

   private ArrayList<Weight> _weights;


   ////////////////////////////////////////////////////////////////////////////////
   /// CTOR: construct an object from name + id.
   ////////////////////////////////////////////////////////////////////////////////
   public Profile( String firstName, String surName, int id  )
   {
      _firstName = firstName;
      _surName = surName;
      _id      = id;
      _weights = new ArrayList<Weight>();
   }

   ////////////////////////////////////////////////////////////////////////////////
   /// CTOR: Construct an object from a JSONObject
   ////////////////////////////////////////////////////////////////////////////////
   public Profile( JSONObject object ) throws JSONException {
      _firstName = object.getString("_firstName");
      _surName = object.getString("_surName");
      _id   = object.getInt( "_id" );

      _weights = new ArrayList<Weight>();
      JSONArray weights = object.getJSONArray( "_weights" );
      for (int i=0; i<weights.length(); i++ )
      {
         Weight w = new Weight( (weights.getJSONObject( i ) ));
         _weights.add( w );
      }
   }

   ////////////////////////////////////////////////////////////////////////////////
   /// Serialize an object to a JSONObject.
   ////////////////////////////////////////////////////////////////////////////////
   public JSONObject toJSON() throws JSONException {
      JSONObject jobj = new JSONObject();

      jobj.put( "_firstName", _firstName);
      jobj.put( "_surName", _surName);
      jobj.put( "_id", _id );

      JSONArray ja = new JSONArray();
      for ( Weight w : _weights )
      {
         ja.put( w.toJSON() );
      }
      jobj.put( "_weights", ja );

      return jobj;
   }

   ////////////////////////////////////////////////////////////////////////////////
   ///
   ////////////////////////////////////////////////////////////////////////////////
   public void addWeight( double w, String d )
   {
      _weights.add( new Weight(w,d));
   }

   ////////////////////////////////////////////////////////////////////////////////
   ///
   ////////////////////////////////////////////////////////////////////////////////
   public void print()
   {
      System.out.format( "%s %s - ID: %d\n" , _firstName, _surName, _id );
      for ( Weight w : _weights )
      {
         w.print();
      }
   }

}
