import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginFrame extends JFrame implements ActionListener {

    JLabel userLabel = new JLabel("Gebruikersnaam");
    JLabel passwordLabel = new JLabel("Wachtwoord");
    JTextField userTextField = new JTextField();
    JPasswordField passwordField = new JPasswordField();
    JButton loginButton = new JButton("Login");
    JButton resetButton = new JButton("Reset");
    JCheckBox showPassword = new JCheckBox("Toon wachtwoord");

    LoginFrame() {
        setLayoutManager();
        setLocationAndSize();
        addComponentsToContainer();
        addActionEvent();
    }

    public void setLayoutManager() {
        this.setLayout(null);
    }

    public void setLocationAndSize() {
        userLabel.setBounds(50, 150, 100, 30);
        passwordLabel.setBounds(50, 220, 100, 30);
        userTextField.setBounds(150, 150, 150, 30);
        passwordField.setBounds(150, 220, 150, 30);
        showPassword.setBounds(150, 250, 150, 30);
        loginButton.setBounds(50, 300, 100, 30);
        resetButton.setBounds(200, 300, 100, 30);
    }

    public void addComponentsToContainer() {
        this.add(userLabel);
        this.add(passwordLabel);
        this.add(userTextField);
        this.add(passwordField);
        this.add(showPassword);
        this.add(loginButton);
        this.add(resetButton);
    }

    public void addActionEvent() {
        loginButton.addActionListener(this);
        resetButton.addActionListener(this);
        showPassword.addActionListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == loginButton) {
            String gebruikerText;
            String wwText;
            gebruikerText = userTextField.getText();
            wwText = passwordField.getText();
            zorgverlenerGUI zvGUI = new zorgverlenerGUI();
            patientGUI ptGUI = new patientGUI();
            if(gebruikerText.equalsIgnoreCase("Jeroen") && wwText.equalsIgnoreCase("12345")) {
                    JOptionPane.showMessageDialog(this, "Succesvolle login!");
                    dispose();
                    zvGUI.zvFrame();
            } else if(gebruikerText.equalsIgnoreCase("1") && wwText.equalsIgnoreCase("jaap")) {
                    JOptionPane.showMessageDialog(this, "Succesvolle login!");
                    dispose();
                    ptGUI.PatientGUI();
            } else{
                JOptionPane.showMessageDialog(this, "Verkeerde gebruikersnaam of wachtwoord");;
            }
        }
        if (e.getSource() == resetButton) {
            userTextField.setText("");
            passwordField.setText("");
        }
        if (e.getSource() == showPassword) {
            if (showPassword.isSelected()) {
                passwordField.setEchoChar((char) 0);
            } else {
                passwordField.setEchoChar('*');
            }
        }
    }
}

