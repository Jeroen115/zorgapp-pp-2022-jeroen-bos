public class Fysio {

    Patients patients;
    Patient  patient;
    boolean fysio;


    Patient selectPatientMenu()
    {
        patients.writeOneliners();
        System.out.println( "enter Patient ID:" );
        var scanner = new BScanner();

        int id = scanner.scanInt();
        return patients.isValidId( id ) ? patients.getPatient( id ) : patient;
    }


    void FysioMenu()
    {
        final int STOP   = 0;
        final int SELECT = 1;
        final int PRINT  = 2;
        final int PLOTW  = 3;
        final int EDIT   = 4;

        var scanner = new BScanner();


        int choice = 1;
        while (choice != 0)
        {
            if (fysio)
            {
                System.out.format( "%s\n", "=".repeat( 80 ) );
                System.out.format( "Current patient: " );
                patient.writeOneliner();
            }


            System.out.format( "%d:  STOP\n", STOP );
            if (fysio)
            {
                System.out.format( "%d:  Select Patient\n", SELECT );
            }
            System.out.format( "%d:  Print patient data\n", PRINT );
            System.out.format( "%d:  Plot  patient weights\n", PLOTW );
            System.out.format( "%d:  Edit  patient data\n", EDIT );


            System.out.println( "enter digit:" );
            choice = scanner.scanInt();
            switch (choice)
            {
                case STOP:
                    break;

                case SELECT:
                    if (fysio)
                    {
                        patient = selectPatientMenu();
                    }
                    break;

                case PRINT:
                    patient.write();
                    break;

                case EDIT:
                    patient.editMenu( fysio );
                    break;

                case PLOTW:
                    patient.plotWeights();
                    break;

                default:
                    System.out.println( "Please enter a *valid* digit" );
                    break;
            }
        }








    }














}
